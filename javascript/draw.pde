void draw(){
	if (!pause) {
		pushMatrix();
		translate(width/2-player.pos.x, height/2-player.pos.y);
		m.run();
		particles.runParticles();
		cells.run();
		popMatrix();
	}
	player.stats();
 };
 void mouseMoved() {
 	player.control();
 };
