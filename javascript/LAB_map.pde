var LAB_map = function(R) {
	this.radius = R;
};
LAB_map.prototype.draw = function() {
	background(200, 10, 10);
	fill(255, 0, 0);
	strokeWeight(10);
	stroke(190, 30, 30);
	ellipse(0, 0, this.radius*2, this.radius*2);
};
LAB_map.prototype.update = function() {
	//control the perlin variable
	perlin+=0.01;
};
LAB_map.prototype.run = function() {
	this.update();
	this.draw();
}
var m = new LAB_map(600);