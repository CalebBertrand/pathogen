var particle = function(X, Y, C, Type, Fun) {
	this.pos = new PVector(X, Y);
	this.vel = new PVector(0, 0);
	this.accel = new PVector(0, 0);
	this.color = C;
	this.isDead = false;
	this.type = Type;
	this.affect = Fun;
};
particle.prototype.physics = function() {
	this.vel.add(this.accel);
	this.vel.limit(2);
	this.pos.add(this.vel);
	this.pos.limit(m.radius);
};
//A simple drawing prototype that creates a circle of the wanted color. Only used if the specific particle has no other drawing methods
particle.prototype.draw = function() {
	noStroke();
	fill(this.color);
	ellipse(this.pos.x, this.pos.y, 6, 6);
};
particle.prototype.checkForCollision = function(target) {
	var d = PVector.sub(this.pos, target.pos);
	if (d.mag() < target.s) {
		return true;
	}
};
particle.prototype.run = function() {
		var d = dist(this.pos.x, this.pos.y, player.pos.x, player.pos.y);
		if (d < width/1.5) {
			if (d < player.pm/6 + player.s) {
				if (this.checkForCollision(player)) {
					this.isDead = true;
					this.affect();
				}else{
					this.attract(player, player.pm/10);
					this.physics();
				}
			}
			this.draw();
		}
};
particle.prototype.attract = function(target, pow) {
	//resets the vel and accel of the particle
	this.vel.mult(0.2);
	this.accel.mult(0.2);

	var force = PVector.sub(this.pos, target.pos);
    var distance = force.mag();
    var strength = pow*2 / (distance);
    force.normalize();
    force.mult(strength);
    if (this.type !== "Toxic") {
    	this.accel.sub(force);
    }else{
    	this.accel.add(force);
    }
};