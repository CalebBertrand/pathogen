var chemical = function(X, Y, C, Type, Fun) {
	this.pos = new PVector(X, Y);
	this.vel = new PVector(0, 0);
	this.accel = new PVector(0, 0);
	this.color = C;
	this.isDead = false;
	this.type = Type;
	this.affect = Fun;
};
chemical.prototype = Object.create(particle.prototype);
chemical.prototype.draw = function() {
	noStroke();
	fill(this.color);
	pushMatrix();
	translate(this.pos.x, this.pos.y);
	rotate(perlin*2);
	beginShape();
	for (var angle = 0; angle < TWO_PI; angle+=TWO_PI/5) { 
		var x = cos(angle)*5.5;
		var y = sin(angle)*5.5;
		vertex(x, y);
	}
	endShape();
	popMatrix();
};

var particles = [];
for (var i = 0; i < 17; i++) {
	particles.push(new particle(cos(random(0, TWO_PI))*random(1, m.radius*0.9), sin(random(0, TWO_PI))*random(1, m.radius), color(255, 234, 0), "Glucose", function() {
		player.glucose++;
	}));
}
for (var i = 0; i < 5; i++) {
	particles.push(new particle(cos(random(0, TWO_PI))*random(1, m.radius), sin(random(0, TWO_PI))*random(1, m.radius), color(10, 255, 0), "Toxic", function() {
		player.hp--;
	}));
	particles.push(new chemical(cos(random(0, TWO_PI))*random(1, m.radius), sin(random(0, TWO_PI))*random(1, m.radius), color(230, 130, 249), "Chemical", function() {
		player.chem++;
	}));
}