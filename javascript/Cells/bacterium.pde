var bacterium = function(X, Y, velL, S) {
	this.pos = new PVector(X, Y);
	this.vel = new PVector(0, 0);
	this.accel = new PVector(random(-0.01, 0.01), random(-0.01, 0.01));
	this.velLimit = velL;
	this.s = S;
	this.isDead = false;

	this.hp = 100;
	//plasma membrane
	this.pm = 200;
	//cell wall
	this.cw = 10;
	//ribosomes
	this.ribo = 10;
	//ammount of glucose
	this.glucose = 0;
	//ammount of energy
	this.energy = 0;
	//ammount of chemicals
	this.chem = 0;
};
bacterium.prototype = Object.create(cell.prototype);

bacterium.prototype.draw = function(){
	//Blobbiness and plasma membrane
	var p = 0;
	pushMatrix();
	translate(this.pos.x, this.pos.y);
	strokeWeight(this.s/19);
	stroke(20, 210, 60, 170);
	fill(255, 0, 0);
	beginShape();
	for (var angle = 0; angle < TWO_PI; angle+=9/this.s){ 
		var d = map(noise(p, frameCount/100), 0, 1, this.s*0.7, this.s);
		var x = cos(angle)*d;
		var y = sin(angle)*d;
		vertex(x, y);
		p+=0.1;
	}
	endShape(CLOSE);
	popMatrix();
};

for (var i = 0; i < 3; i++) {
	cells.push(new bacterium(random(-m.radius/1.5, m.radius/1.5), random(-m.radius/1.5, m.radius/1.5), 2.5, random(20, 60)));
}
var player = new bacterium(0, 0, 2.5, 25);
cells.push(player);