Object.prototype.LAB_apply = function(pvec) {
	this.accel.add(pvec);
};

var cells = [];

var cell = function(X, Y, velL, S) {
	this.pos = new PVector(X, Y);
	this.vel = new PVector(0, 0);
	this.accel = new PVector(random(-0.01, 0.01), random(-0.01, 0.01));
	this.velLimit = velL;
	this.s = S;
	this.isDead = false;
	this.hp = 100;
};
cell.prototype.physics = function() {
	this.vel.add(this.accel);
	this.vel.limit(this.velLimit);
	this.pos.add(this.vel);
	this.pos.limit(m.radius-this.s);
};
cell.prototype.run = function() {
	this.physics();
	var d = dist(this.pos.x, this.pos.y, player.pos.x, player.pos.y);
	if (d < width/1.6) {
		this.draw();
	}
	if (this.hp < 0) {
		this.isDead = true;
	}
};