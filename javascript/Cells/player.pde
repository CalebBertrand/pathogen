cell.prototype.control = function() {
		var d = new PVector(mouseX-width/2, mouseY-height/2);
		d.div(25*this.s);
		this.vel.mult(0.95);
		this.vel.add(d);
		this.vel.limit(this.velLimit);
};
cell.prototype.stats = function() {
	if (this.glucose > 0) {
		this.glucose-=0.01;
		this.energy+=0.01;
		this.energy-=0.006;
		this.s+=0.001;
	}

	pushMatrix();
	//transparent dark background
	noStroke();
	fill(30, 30, 30, 135);
	rect(0, 0, width, 45);

	//hp bar
	fill(255, 0, 0);
	rect(0, 0, this.hp, 10);
	//energy bar
	fill(255, map(noise(perlin), 0, 1, 200, 225), 0);
	rect(0, 10, this.energy*3, 10);
	//chem bar
	fill(230, 130, 249);
	rect(0, 20, this.chem*2, 10);
};
cell.prototype.reproduce = function() {
	if (this.s > 50) {
		this.s/=2;
		cells.push(new bacterium(this.pos.x, this.pos.y, this.velLimit, this.s/2));
	}
};